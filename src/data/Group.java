package data;

public class Group {

    private String groupCode;

    public Group(String groupCode) {
        this.groupCode = groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getGroupCode() {
        return groupCode;
    }
}
