package data;

import java.util.HashMap;

public class StudentProgress {

    private HashMap<Discipline, Integer> marks;

    public HashMap<Discipline, Integer> getMarks() {
        return marks;
    }

    public void setMarks(HashMap<Discipline, Integer> marks) {
        this.marks = marks;
    }



}
